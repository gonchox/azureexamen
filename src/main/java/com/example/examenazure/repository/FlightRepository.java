package com.example.examenazure.repository;

import com.example.examenazure.model.Flight;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface FlightRepository extends JpaRepository<Flight, Integer> {
   Flight getFlightByFlightNumber(int flightNumber);
}
