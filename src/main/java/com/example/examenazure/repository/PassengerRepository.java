package com.example.examenazure.repository;

import com.example.examenazure.model.Passenger;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PassengerRepository extends JpaRepository<Passenger, Integer> {
   Passenger getPassangerByName(String name);
}
