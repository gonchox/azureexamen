package com.example.examenazure.dto;

import lombok.Data;

@Data
public class PassengerDTO {
    private String name;
    private String phone;
    private String email;
}
