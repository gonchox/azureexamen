package com.example.examenazure.dto;

import lombok.Data;

@Data
public class FlightDTO {
    private int flightNumber;
    private String origin;
    private String destiny;
    private int idAirline;
}
