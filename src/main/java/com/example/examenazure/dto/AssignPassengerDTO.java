package com.example.examenazure.dto;

import lombok.Data;

@Data
public class AssignPassengerDTO {
    private String passengerName;
    private int flightNumber;
}
