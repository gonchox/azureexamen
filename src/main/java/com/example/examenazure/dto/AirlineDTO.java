package com.example.examenazure.dto;

import lombok.Data;

@Data
public class AirlineDTO {
    private String name;
}
