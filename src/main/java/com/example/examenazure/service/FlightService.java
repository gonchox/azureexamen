package com.example.examenazure.service;

import com.example.examenazure.dto.FlightDTO;
import com.example.examenazure.model.Flight;

import java.util.List;
import java.util.Optional;

public interface FlightService {
    List<Flight> getFlights();
    Flight createFlight(FlightDTO flightDTO);
    Flight getFlightByFlightNumber(int flightNumber);
}
