package com.example.examenazure.service.Impl;

import com.example.examenazure.dto.AssignPassengerDTO;
import com.example.examenazure.dto.PassengerDTO;
import com.example.examenazure.model.Flight;
import com.example.examenazure.model.Passenger;
import com.example.examenazure.repository.FlightRepository;
import com.example.examenazure.repository.PassengerRepository;
import com.example.examenazure.service.PassengerService;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PassengerServiceImpl implements PassengerService {
    @Autowired
    private PassengerRepository passengerRepository;
    @Autowired
    private FlightRepository flightRepository;

    @Override
    public List<Passenger> getPassengers() {
        return passengerRepository.findAll();
    }

    @Override
    public Passenger createPassenger(PassengerDTO passengerDTO) {
        Passenger passenger = new Passenger(passengerDTO);
        return passengerRepository.save(passenger);
    }

    @Override
    public Passenger getPassengerByName(String name) {
        return passengerRepository.getPassangerByName(name);
    }

    @Override
    public Passenger assignPassengerToFlight(AssignPassengerDTO assignPassengerDTO) {
        String passengerName = assignPassengerDTO.getPassengerName();
        int flightNumber = assignPassengerDTO.getFlightNumber();

        Passenger passenger = passengerRepository.getPassangerByName(passengerName);
        Flight flight = flightRepository.getFlightByFlightNumber(flightNumber);

        passenger.setFlight(flight);
        flight.getPassengerList().add(passenger);
        flightRepository.save(flight);

        return passenger;
    }
}
