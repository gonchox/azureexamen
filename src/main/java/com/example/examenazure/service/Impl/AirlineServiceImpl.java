package com.example.examenazure.service.Impl;

import com.example.examenazure.dto.AirlineDTO;
import com.example.examenazure.model.Airline;
import com.example.examenazure.repository.AirlineRepository;
import com.example.examenazure.service.AirlineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AirlineServiceImpl implements AirlineService {

    @Autowired
    private AirlineRepository airlineRepository;

    @Override
    public List<Airline> getAirlines() {
        return (List<Airline>) airlineRepository.findAll();
    }

    @Override
    public Airline createAirline(AirlineDTO airlineDTO) {
        Airline airline = new Airline(airlineDTO);
        return airlineRepository.save(airline);
    }

    @Override
    public Optional<Airline> getAirlineById(int id) {
        return airlineRepository.findById(id);
    }
}
