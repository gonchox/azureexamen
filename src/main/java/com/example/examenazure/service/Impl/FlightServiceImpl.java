package com.example.examenazure.service.Impl;

import com.example.examenazure.dto.FlightDTO;
import com.example.examenazure.model.Airline;
import com.example.examenazure.model.Flight;
import com.example.examenazure.repository.AirlineRepository;
import com.example.examenazure.repository.FlightRepository;
import com.example.examenazure.service.FlightService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class FlightServiceImpl implements FlightService {

    @Autowired
    private FlightRepository flightRepository;
    @Autowired
    private AirlineRepository airlineRepository;

    @Override
    public List<Flight> getFlights() {
        return flightRepository.findAll();
    }

    @Override
    public Flight createFlight(FlightDTO flightDTO) {
        Optional<Airline> airlineOptional = airlineRepository.findById(flightDTO.getIdAirline());
        Airline airline = airlineOptional.get();

        Flight flight = new Flight();
        flight.setIdAirline(airline.getId());
        flight.setFlightNumber(flightDTO.getFlightNumber());
        flight.setOrigin(flightDTO.getOrigin());
        flight.setDestiny(flightDTO.getDestiny());

        return flightRepository.save(flight);
    }

    @Override
    public Flight getFlightByFlightNumber(int flightNumber) {
        return flightRepository.getFlightByFlightNumber(flightNumber);
    }
}
