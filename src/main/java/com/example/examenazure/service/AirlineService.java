package com.example.examenazure.service;

import com.example.examenazure.dto.AirlineDTO;
import com.example.examenazure.model.Airline;

import java.util.List;
import java.util.Optional;

public interface AirlineService {
    List<Airline> getAirlines();
    Airline createAirline(AirlineDTO airlineDTO);
    Optional<Airline> getAirlineById(int id);
}
