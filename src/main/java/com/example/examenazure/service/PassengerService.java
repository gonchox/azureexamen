package com.example.examenazure.service;

import com.example.examenazure.dto.AssignPassengerDTO;
import com.example.examenazure.dto.PassengerDTO;
import com.example.examenazure.model.Passenger;

import java.util.List;
import java.util.Optional;

public interface PassengerService {
    List<Passenger> getPassengers();
    Passenger createPassenger(PassengerDTO passengerDTO);
    Passenger getPassengerByName(String name);

    Passenger assignPassengerToFlight(AssignPassengerDTO assignPassengerDTO);
}
