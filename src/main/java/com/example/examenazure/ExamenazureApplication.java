package com.example.examenazure;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExamenazureApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExamenazureApplication.class, args);
	}

}
