package com.example.examenazure.model;

import com.example.examenazure.dto.FlightDTO;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;


@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Flight {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private int idAirline;
    private int flightNumber;
    private String origin;
    private String destiny;

    @JsonIgnoreProperties("flight")
    @OneToMany(mappedBy = "flight")
    private List<Passenger> passengerList;

    public Flight(FlightDTO flightDTO){
        this.idAirline = flightDTO.getIdAirline();
        this.flightNumber = flightDTO.getFlightNumber();
        this.origin = flightDTO.getOrigin();
        this.destiny = flightDTO.getDestiny();
    }
}
