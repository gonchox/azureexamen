package com.example.examenazure.model;

import com.example.examenazure.dto.AirlineDTO;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Airline {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String name;

    public Airline(AirlineDTO airlineDTO){
        this.name = airlineDTO.getName();
    }
}
