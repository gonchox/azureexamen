package com.example.examenazure.model;

import com.example.examenazure.dto.PassengerDTO;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "passenger")
public class Passenger {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne
    @JsonIgnoreProperties({"passengers","passengerList"})
    @JoinColumn(name = "idFlight")
    private Flight flight;
    private String name;
    private String phone;
    private String email;

    public Passenger(PassengerDTO passengerDTO){
        this.name = passengerDTO.getName();
        this.phone = passengerDTO.getPhone();
        this.email = passengerDTO.getEmail();
    }
}
