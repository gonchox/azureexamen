package com.example.examenazure.controller;

import com.example.examenazure.dto.AirlineDTO;
import com.example.examenazure.model.Airline;
import com.example.examenazure.service.AirlineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/v1/airlines")
public class AirlineController {

    @Autowired
    private AirlineService airlineService;

    @PostMapping("")
    public ResponseEntity<Airline> createAirline(@RequestBody AirlineDTO airlineDTO){
        try{
            Airline airline = airlineService.createAirline(airlineDTO);
            return ResponseEntity.status(HttpStatus.CREATED).body(airline);
        }catch (Exception e){
            return ResponseEntity.internalServerError().build();
        }
    }

    @GetMapping("")
    public ResponseEntity<List<Airline>> getAirlines(){
        try{
            List<Airline> airlines = airlineService.getAirlines();
            return ResponseEntity.status(HttpStatus.OK).body(airlines);
        }catch (Exception e){
            return ResponseEntity.internalServerError().build();
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<Optional<Airline>> getAirlineById(@PathVariable int id){
        try{
            Optional<Airline> airline = airlineService.getAirlineById(id);
            return ResponseEntity.status(HttpStatus.OK).body(airline);
        }catch (Exception e){
            return ResponseEntity.internalServerError().build();
        }
    }
}
