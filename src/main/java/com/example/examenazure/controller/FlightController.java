package com.example.examenazure.controller;

import com.example.examenazure.dto.AirlineDTO;
import com.example.examenazure.dto.FlightDTO;
import com.example.examenazure.model.Airline;
import com.example.examenazure.model.Flight;
import com.example.examenazure.service.FlightService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/v1/flights")
public class FlightController {
    @Autowired
    private FlightService flightService;

    @PostMapping("")
    public ResponseEntity<Flight> createFlight(@RequestBody FlightDTO flightDTO){
        try{
            Flight flight = flightService.createFlight(flightDTO);
            return ResponseEntity.status(HttpStatus.CREATED).body(flight);
        }catch (Exception e){
            return ResponseEntity.internalServerError().build();
        }
    }

    @GetMapping("")
    public ResponseEntity<List<Flight>> getFlights(){
        try{
            List<Flight> flights = flightService.getFlights();
            return ResponseEntity.status(HttpStatus.OK).body(flights);
        }catch (Exception e){
            return ResponseEntity.internalServerError().build();
        }
    }

    @GetMapping("/{flightNumber}")
    public ResponseEntity<Flight> getFlightByFlightNumber(@PathVariable int flightNumber){
        try{
            Flight flight = flightService.getFlightByFlightNumber(flightNumber);
            return ResponseEntity.status(HttpStatus.OK).body(flight);
        }catch (Exception e){
            return ResponseEntity.internalServerError().build();
        }
    }
}
