package com.example.examenazure.controller;

import com.example.examenazure.dto.AirlineDTO;
import com.example.examenazure.dto.AssignPassengerDTO;
import com.example.examenazure.dto.PassengerDTO;
import com.example.examenazure.model.Airline;
import com.example.examenazure.model.Passenger;
import com.example.examenazure.service.PassengerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/v1/passengers")
public class PassengerController {
    @Autowired
    private PassengerService passengerService;

    @PostMapping("")
    public ResponseEntity<Passenger> createPassenger(@RequestBody PassengerDTO passengerDTO){
        try{
            Passenger passenger = passengerService.createPassenger(passengerDTO);
            return ResponseEntity.status(HttpStatus.CREATED).body(passenger);
        }catch (Exception e){
            return ResponseEntity.internalServerError().build();
        }
    }

    @GetMapping("")
    public ResponseEntity<List<Passenger>> getPassengers(){
        try{
            List<Passenger> passengers = passengerService.getPassengers();
            return ResponseEntity.status(HttpStatus.OK).body(passengers);
        }catch (Exception e){
            return ResponseEntity.internalServerError().build();
        }
    }

    @GetMapping("/{name}")
    public ResponseEntity<Passenger> getPassengerByName(@PathVariable String name){
        try{
           Passenger passenger = passengerService.getPassengerByName(name);
            return ResponseEntity.status(HttpStatus.OK).body(passenger);
        }catch (Exception e){
            return ResponseEntity.internalServerError().build();
        }
    }

    @PutMapping("")
    public ResponseEntity<Passenger> assignPassengerToFlight(@RequestBody AssignPassengerDTO assignPassengerDTO) {
        Passenger passenger = passengerService.assignPassengerToFlight(assignPassengerDTO);
        return ResponseEntity.ok(passenger);
    }
}
