FROM openjdk:17

WORKDIR /app

COPY ./target/examenazure-0.0.1-SNAPSHOT.jar .

EXPOSE 8080

ENTRYPOINT ["java", "-jar", "examenazure-0.0.1-SNAPSHOT.jar"]